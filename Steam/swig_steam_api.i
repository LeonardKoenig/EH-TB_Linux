// We need the director class for the abstract CCallbackBase
%module(directors="1", allprotected="1") steam_api
%feature("director") CCallbackBase;

// Verbatim copy this: include this file in wrapper
%{
#include "steam_api.h"
%}

// Now process the main header
%include "steam_api.h" 

// load the binaries on startup
%pragma(java) jniclasscode=%{
  static {
    try {
      if (System.getProperty("sun.arch.data.model").contains("32")) {
        System.loadLibrary("steam_api");
        System.loadLibrary("SteamJavaWrapper");
      }
      else {
        System.loadLibrary("steam_api64");
        System.loadLibrary("SteamJavaWrapper64");
      }
    }
    catch (UnsatisfiedLinkError unsatisfiedLinkError) {
      System.err.println("Native code library failed to load. \n" + unsatisfiedLinkError);
      System.exit(1);
    }
    swig_module_init();
  }
%}

// vim: set ft=swig ts=8 sw=2 textwidth=100 et :
