using EdnaStartCS.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace EdnaStartCS
{
	public class EdnaForm : Form
	{
		public static bool enableComments;

		private IContainer components;

		private Button buttonStart;

		private Button buttonQuit;

		private RadioButton fullscreen;

		private RadioButton window;

		private RadioButton langEN;

		private RadioButton langDE;

		private Label labelScreen;

		private Label labelLanguage;

		private CheckBox checkBoxMusic;

		private CheckBox checkBoxSpeech;

		private CheckBox checkBoxSubtitle;

		private GroupBox groupBoxLanguage;

		private GroupBox groupBoxScreenModus;

		private CheckBox checkBoxComment;

		private CheckBox checkBoxHardwareAccelerated;

		public EdnaForm()
		{
			this.InitializeComponent();
			this.setStringsToCurrentLanguage();
			this.setActiveControls();
			if (!EdnaForm.enableComments)
			{
				this.checkBoxComment.Visible = false;
			}
		}

		private string computeJavaPath()
		{
			string environmentVariable = Environment.GetEnvironmentVariable("JDK_HOME");
			if (environmentVariable == null)
			{
				environmentVariable = Environment.GetEnvironmentVariable("JAVA_HOME");
			}
			if (environmentVariable.Length <= 0)
			{
				return "???";
			}
			return environmentVariable;
		}

		private void setActiveControls()
		{
			this.langDE.Checked = (Program.isGerman);
			this.langEN.Checked = (!this.langDE.Checked);
			this.fullscreen.Checked = (Program.isFullScreen);
			this.window.Checked = (!this.fullscreen.Checked);
			this.checkBoxMusic.Checked = (Program.isMusicOn);
			this.checkBoxSpeech.Checked = (Program.isSpeechOn);
			this.checkBoxSubtitle.Checked = (Program.isSubtitleOn);
			this.checkBoxComment.Checked = (Program.isCommentOn);
			this.checkBoxHardwareAccelerated.Checked = (Program.isHardwareAccelerated);
		}

		private void setStringsToCurrentLanguage()
		{
			if (Program.isGerman)
			{
				this.setStringsToGerman();
				return;
			}
			this.setStringsToEnglish();
		}

		private void setStringsToGerman()
		{
			this.groupBoxLanguage.Text = (Resources.gbLanguage_de);
			this.groupBoxScreenModus.Text = (Resources.gbScreenModus_de);
			this.fullscreen.Text = (Resources.rbFullscreen_de);
			this.window.Text = (Resources.rbWindow_de);
			this.checkBoxMusic.Text = (Resources.cbMusic_de);
			this.checkBoxSpeech.Text = (Resources.cbSpeech_de);
			this.checkBoxSubtitle.Text = (Resources.cbSubtitle_de);
			this.checkBoxComment.Text = (Resources.cbComment_de);
			this.checkBoxHardwareAccelerated.Text = (Resources.cbHardwareAccerlated_de);
			this.buttonStart.Text = (Resources.btnStart_de);
			this.buttonQuit.Text = (Resources.btnQuit_de);
		}

		private void setStringsToEnglish()
		{
			this.groupBoxLanguage. Text = (Resources.gbLanguage_en);
			this.groupBoxScreenModus. Text = (Resources.gbScreenModus_en);
			this.fullscreen. Text = (Resources.rbFullscreen_en);
			this.window. Text = (Resources.rbWindow_en);
			this.checkBoxMusic. Text = (Resources.cbMusic_en);
			this.checkBoxSpeech. Text = (Resources.cbSpeech_en);
			this.checkBoxSubtitle. Text = (Resources.cbSubtitle_en);
			this.checkBoxComment. Text = (Resources.cbComment_en);
			this.checkBoxHardwareAccelerated. Text = (Resources.cbHardwareAccerlated_en);
			this.buttonStart. Text = (Resources.btnStart_en);
			this.buttonQuit. Text = (Resources.btnQuit_en);
		}

		private void EdnaForm_Load(object sender, EventArgs e)
		{
		}

		private void language_Click(object sender, EventArgs e)
		{
		}

		private void start_Click(object sender, EventArgs e)
		{
			Program.writePreferences();
			Program.startEdna();
			Application.Exit();
		}

		private void quit_Click(object sender, EventArgs e)
		{
			Program.writePreferences();
			Application.Exit();
		}

		private void optionJVM_TextChanged(object sender, EventArgs e)
		{
		}

		private void langDE_CheckedChanged(object sender, EventArgs e)
		{
			if (!Program.isGerman)
			{
				this.setStringsToGerman();
				Program.isGerman = true;
				Program.isEnglish = false;
			}
		}

		private void langEN_CheckedChanged(object sender, EventArgs e)
		{
			if (!Program.isEnglish)
			{
				this.setStringsToEnglish();
				Program.isGerman = false;
				Program.isEnglish = true;
			}
		}

		private void checkOptionsJVM_CheckedChanged(object sender, EventArgs e)
		{
			MessageBox.Show(Program.isGerman ? Resources.msgOptionsJVM_de : Resources.msgOptionsJVM_en, "!", MessageBoxButtons.OK);
		}

		private void checkBoxMusic_CheckedChanged(object sender, EventArgs e)
		{
			Program.isMusicOn = this.checkBoxMusic.Checked;
		}

		private void checkBoxSpeech_CheckedChanged(object sender, EventArgs e)
		{
			Program.isSpeechOn = this.checkBoxSpeech.Checked;
		}

		private void checkBoxText_CheckedChanged(object sender, EventArgs e)
		{
			Program.isSubtitleOn = this.checkBoxSubtitle.Checked;
		}

		private void checkBoxComment_CheckedChanged(object sender, EventArgs e)
		{
			Program.isCommentOn = this.checkBoxComment.Checked;
		}

		private void window_CheckedChanged(object sender, EventArgs e)
		{
			if (!this.fullscreen.Checked && !Program.ignoreWindowWarning)
			{
				if (Program.isGerman)
				{
					MessageBox.Show(Resources.msgWindowWarn_de, Resources.msgWindowTitle_de, 0, MessageBoxIcon.Warning);
				}
				if (Program.isEnglish)
				{
					MessageBox.Show(Resources.msgWindowWarn_en, Resources.msgWindowTitle_en, 0, MessageBoxIcon.Warning);
				}
				Program.ignoreWindowWarning = true;
			}
			Program.isFullScreen = this.fullscreen.Checked;
		}

		private void fullscreen_CheckedChanged(object sender, EventArgs e)
		{
			Program.isFullScreen = this.fullscreen.Checked;
		}

		private void checkBoxHardwareAccelerated_CheckedChanged(object sender, EventArgs e)
		{
			Program.isHardwareAccelerated = this.checkBoxHardwareAccelerated.Checked;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(EdnaForm));
			this.buttonStart = new Button();
			this.buttonQuit = new Button();
			this.fullscreen = new RadioButton();
			this.window = new RadioButton();
			this.langEN = new RadioButton();
			this.langDE = new RadioButton();
			this.labelScreen = new Label();
			this.labelLanguage = new Label();
			this.checkBoxMusic = new CheckBox();
			this.checkBoxSpeech = new CheckBox();
			this.checkBoxSubtitle = new CheckBox();
			this.groupBoxLanguage = new GroupBox();
			this.groupBoxScreenModus = new GroupBox();
			this.checkBoxComment = new CheckBox();
			this.checkBoxHardwareAccelerated = new CheckBox();
			this.groupBoxLanguage.SuspendLayout();
			this.groupBoxScreenModus.SuspendLayout();
			base.SuspendLayout();
			this.buttonStart.Location = (new Point(346, 362));
			this.buttonStart.Name = ("buttonStart");
			this.buttonStart.Size = (new Size(102, 27));
			this.buttonStart.TabIndex = (0);
			this.buttonStart.UseVisualStyleBackColor = (true);
			this.buttonStart.Click += (new EventHandler(this.start_Click));
			this.buttonQuit.Location = (new Point(481, 362));
			this.buttonQuit.Name = ("buttonQuit");
			this.buttonQuit.Size = (new Size(102, 27));
			this.buttonQuit.TabIndex = (1);
			this.buttonQuit.UseVisualStyleBackColor = (true);
			this.buttonQuit.Click += (new EventHandler(this.quit_Click));
			this.fullscreen.AutoSize = (true);
			this.fullscreen.Location = (new Point(17, 21));
			this.fullscreen.Name = ("fullscreen");
			this.fullscreen.Size = (new Size(14, 13));
			this.fullscreen.TabIndex = (2);
			this.fullscreen.TabStop = (true);
			this.fullscreen.UseVisualStyleBackColor = (true);
			this.fullscreen.CheckedChanged += (new EventHandler(this.fullscreen_CheckedChanged));
			this.window.AutoSize = (true);
			this.window.ForeColor = (Color.Red);
			this.window.Location = (new Point(17, 48));
			this.window.Name = ("window");
			this.window.Size = (new Size(14, 13));
			this.window.TabIndex = (3);
			this.window.TabStop = (true);
			this.window.UseVisualStyleBackColor = (true);
			this.window.CheckedChanged += (new EventHandler(this.window_CheckedChanged));
			this.langEN.AutoSize = (true);
			this.langEN.Location = (new Point(17, 44));
			this.langEN.Name = ("langEN");
			this.langEN.Size = (new Size(58, 17));
			this.langEN.TabIndex = (4);
			this.langEN.TabStop = (true);
			this.langEN.Text = ("english");
			this.langEN.UseVisualStyleBackColor = (true);
			this.langEN.CheckedChanged += (new EventHandler(this.langEN_CheckedChanged));
			this.langDE.AutoSize = (true);
			this.langDE.Location = (new Point(17, 19));
			this.langDE.Name = ("langDE");
			this.langDE.Size = (new Size(63, 17));
			this.langDE.TabIndex = (5);
			this.langDE.TabStop = (true);
			this.langDE.Text = ("deutsch");
			this.langDE.UseVisualStyleBackColor = (true);
			this.langDE.CheckedChanged += (new EventHandler(this.langDE_CheckedChanged));
			this.labelScreen.AutoSize = (true);
			this.labelScreen.Location = (new Point(363, 42));
			this.labelScreen.Name = ("labelScreen");
			this.labelScreen.Size = (new Size(0, 13));
			this.labelScreen.TabIndex = (8);
			this.labelLanguage.AutoSize = (true);
			this.labelLanguage.Location = (new Point(485, 42));
			this.labelLanguage.Name = ("labelLanguage");
			this.labelLanguage.Size = (new Size(0, 13));
			this.labelLanguage.TabIndex = (9);
			this.labelLanguage.Click += (new EventHandler(this.language_Click));
			this.checkBoxMusic.AutoSize = (true);
			this.checkBoxMusic.BackColor = (Color.Transparent);
			this.checkBoxMusic.Location = (new Point(354, 160));
			this.checkBoxMusic.Name = ("checkBoxMusic");
			this.checkBoxMusic.Size = (new Size(15, 14));
			this.checkBoxMusic.TabIndex = (11);
			this.checkBoxMusic.UseVisualStyleBackColor = (false);
			this.checkBoxMusic.CheckedChanged += (new EventHandler(this.checkBoxMusic_CheckedChanged));
			this.checkBoxSpeech.AutoSize = (true);
			this.checkBoxSpeech.BackColor = (Color.Transparent);
			this.checkBoxSpeech.Location = (new Point(354, 200));
			this.checkBoxSpeech.Name = ("checkBoxSpeech");
			this.checkBoxSpeech.Size = (new Size(15, 14));
			this.checkBoxSpeech.TabIndex = (12);
			this.checkBoxSpeech.UseVisualStyleBackColor = (false);
			this.checkBoxSpeech.CheckedChanged += (new EventHandler(this.checkBoxSpeech_CheckedChanged));
			this.checkBoxSubtitle.AutoSize = (true);
			this.checkBoxSubtitle.BackColor = (Color.Transparent);
			this.checkBoxSubtitle.Location = (new Point(354, 240));
			this.checkBoxSubtitle.Name = ("checkBoxSubtitle");
			this.checkBoxSubtitle.Size = (new Size(15, 14));
			this.checkBoxSubtitle.TabIndex = (13);
			this.checkBoxSubtitle.UseVisualStyleBackColor = (false);
			this.checkBoxSubtitle.CheckedChanged += (new EventHandler(this.checkBoxText_CheckedChanged));
			this.groupBoxLanguage.BackColor = (Color.Transparent);
			this.groupBoxLanguage.Controls.Add(this.langDE);
			this.groupBoxLanguage.Controls.Add(this.langEN);
			this.groupBoxLanguage.Location = (new Point(346, 58));
			this.groupBoxLanguage.Name = ("groupBoxLanguage");
			this.groupBoxLanguage.Size = (new Size(102, 77));
			this.groupBoxLanguage.TabIndex = (14);
			this.groupBoxLanguage.TabStop = (false);
			this.groupBoxScreenModus.BackColor = (Color.Transparent);
			this.groupBoxScreenModus.Controls.Add(this.window);
			this.groupBoxScreenModus.Controls.Add(this.fullscreen);
			this.groupBoxScreenModus.Location = (new Point(481, 58));
			this.groupBoxScreenModus.Name = ("groupBoxScreenModus");
			this.groupBoxScreenModus.Size = (new Size(102, 77));
			this.groupBoxScreenModus.TabIndex = (15);
			this.groupBoxScreenModus.TabStop = (false);
			this.checkBoxComment.AutoSize = (true);
			this.checkBoxComment.BackColor = (Color.Transparent);
			this.checkBoxComment.Location = (new Point(354, 320));
			this.checkBoxComment.Name = ("checkBoxComment");
			this.checkBoxComment.Size = (new Size(15, 14));
			this.checkBoxComment.TabIndex = (16);
			this.checkBoxComment.UseVisualStyleBackColor = (false);
			this.checkBoxComment.CheckedChanged += (new EventHandler(this.checkBoxComment_CheckedChanged));
			this.checkBoxHardwareAccelerated.AutoSize = (true);
			this.checkBoxHardwareAccelerated.BackColor = (Color.Transparent);
			this.checkBoxHardwareAccelerated.Location = (new Point(354, 280));
			this.checkBoxHardwareAccelerated.Name = ("checkBoxHardwareAccelerated");
			this.checkBoxHardwareAccelerated.Size = (new Size(15, 14));
			this.checkBoxHardwareAccelerated.TabIndex = (17);
			this.checkBoxHardwareAccelerated.UseVisualStyleBackColor = (false);
			this.checkBoxHardwareAccelerated.CheckedChanged += (new EventHandler(this.checkBoxHardwareAccelerated_CheckedChanged));
			base.AutoScaleDimensions = (new SizeF(6f, 13f));
			base.AutoScaleMode = AutoScaleMode.None;//TODO (1);
			this.BackgroundImage = (Resources.welcomeLogoOut);
			base.ClientSize = (new Size(625, 471));
			base.Controls.Add(this.checkBoxHardwareAccelerated);
			base.Controls.Add(this.groupBoxScreenModus);
			base.Controls.Add(this.checkBoxComment);
			base.Controls.Add(this.groupBoxLanguage);
			base.Controls.Add(this.checkBoxSubtitle);
			base.Controls.Add(this.checkBoxSpeech);
			base.Controls.Add(this.checkBoxMusic);
			base.Controls.Add(this.labelLanguage);
			base.Controls.Add(this.labelScreen);
			base.Controls.Add(this.buttonQuit);
			base.Controls.Add(this.buttonStart);
			base.FormBorderStyle = FormBorderStyle.Fixed3D; //TODO (1);
			base.Icon = ((Icon)componentResourceManager.GetObject("$this.Icon"));
			base.Name = ("EdnaForm");
			base.StartPosition = FormStartPosition.CenterParent; //TODO (1);
			this.Text = ("Edna Bricht Aus - Sammler Edition");
			base.Load += (new EventHandler(this.EdnaForm_Load));
			this.groupBoxLanguage.ResumeLayout(false);
			this.groupBoxLanguage.PerformLayout();
			this.groupBoxScreenModus.ResumeLayout(false);
			this.groupBoxScreenModus.PerformLayout();
			base.ResumeLayout(false);
			base.PerformLayout();
		}
	}
}
