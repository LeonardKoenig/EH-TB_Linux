using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace EdnaStartCS.Properties
{
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode, CompilerGenerated]
	public class Resources
	{
		private static ResourceManager resourceMan;

		private static CultureInfo resourceCulture;

		[EditorBrowsable]
		public static ResourceManager ResourceManager
		{
			get
			{
				if (Resources.resourceMan == null)
				{
					Resources.resourceMan = new ResourceManager("EdnaStartCS.Properties.Resources", typeof(Resources).Assembly);
				}
				return Resources.resourceMan;
			}
		}

		[EditorBrowsable]
		public static CultureInfo Culture
		{
			get
			{
				return Resources.resourceCulture;
			}
			set
			{
				Resources.resourceCulture = value;
			}
		}

		public static string _lWindow
		{
			get
			{
				return Resources.ResourceManager.GetString("_lWindow", Resources.resourceCulture);
			}
		}

		public static string btnCancel_de
		{
			get
			{
				return Resources.ResourceManager.GetString("btnCancel_de", Resources.resourceCulture);
			}
		}

		public static string btnCancel_en
		{
			get
			{
				return Resources.ResourceManager.GetString("btnCancel_en", Resources.resourceCulture);
			}
		}

		public static string btnokay_de
		{
			get
			{
				return Resources.ResourceManager.GetString("btnokay_de", Resources.resourceCulture);
			}
		}

		public static string btnOkay_en
		{
			get
			{
				return Resources.ResourceManager.GetString("btnOkay_en", Resources.resourceCulture);
			}
		}

		public static string btnQuit_de
		{
			get
			{
				return Resources.ResourceManager.GetString("btnQuit_de", Resources.resourceCulture);
			}
		}

		public static string btnQuit_en
		{
			get
			{
				return Resources.ResourceManager.GetString("btnQuit_en", Resources.resourceCulture);
			}
		}

		public static string btnStart_de
		{
			get
			{
				return Resources.ResourceManager.GetString("btnStart_de", Resources.resourceCulture);
			}
		}

		public static string btnStart_en
		{
			get
			{
				return Resources.ResourceManager.GetString("btnStart_en", Resources.resourceCulture);
			}
		}

		public static string cbComment_de
		{
			get
			{
				return Resources.ResourceManager.GetString("cbComment_de", Resources.resourceCulture);
			}
		}

		public static string cbComment_en
		{
			get
			{
				return Resources.ResourceManager.GetString("cbComment_en", Resources.resourceCulture);
			}
		}

		public static string cbHardwareAccerlated_de
		{
			get
			{
				return Resources.ResourceManager.GetString("cbHardwareAccerlated_de", Resources.resourceCulture);
			}
		}

		public static string cbHardwareAccerlated_en
		{
			get
			{
				return Resources.ResourceManager.GetString("cbHardwareAccerlated_en", Resources.resourceCulture);
			}
		}

		public static string cbMusic_de
		{
			get
			{
				return Resources.ResourceManager.GetString("cbMusic_de", Resources.resourceCulture);
			}
		}

		public static string cbMusic_en
		{
			get
			{
				return Resources.ResourceManager.GetString("cbMusic_en", Resources.resourceCulture);
			}
		}

		public static string cbOptionsJVM_de
		{
			get
			{
				return Resources.ResourceManager.GetString("cbOptionsJVM_de", Resources.resourceCulture);
			}
		}

		public static string cbOptionsJVM_en
		{
			get
			{
				return Resources.ResourceManager.GetString("cbOptionsJVM_en", Resources.resourceCulture);
			}
		}

		public static string cbSpeech_de
		{
			get
			{
				return Resources.ResourceManager.GetString("cbSpeech_de", Resources.resourceCulture);
			}
		}

		public static string cbSpeech_en
		{
			get
			{
				return Resources.ResourceManager.GetString("cbSpeech_en", Resources.resourceCulture);
			}
		}

		public static string cbSubtitle_de
		{
			get
			{
				return Resources.ResourceManager.GetString("cbSubtitle_de", Resources.resourceCulture);
			}
		}

		public static string cbSubtitle_en
		{
			get
			{
				return Resources.ResourceManager.GetString("cbSubtitle_en", Resources.resourceCulture);
			}
		}

		public static byte[] ednaPreferen
		{
			get
			{
				return (byte[])Resources.ResourceManager.GetObject("ednaPreferen", Resources.resourceCulture);
			}
		}

		public static string gbLanguage_de
		{
			get
			{
				return Resources.ResourceManager.GetString("gbLanguage_de", Resources.resourceCulture);
			}
		}

		public static string gbLanguage_en
		{
			get
			{
				return Resources.ResourceManager.GetString("gbLanguage_en", Resources.resourceCulture);
			}
		}

		public static string gbScreenModus_de
		{
			get
			{
				return Resources.ResourceManager.GetString("gbScreenModus_de", Resources.resourceCulture);
			}
		}

		public static string gbScreenModus_en
		{
			get
			{
				return Resources.ResourceManager.GetString("gbScreenModus_en", Resources.resourceCulture);
			}
		}

		public static Icon Harvey
		{
			get
			{
				return (Icon)Resources.ResourceManager.GetObject("Harvey", Resources.resourceCulture);
			}
		}

		public static string lJavaVMOptionen_de
		{
			get
			{
				return Resources.ResourceManager.GetString("lJavaVMOptionen_de", Resources.resourceCulture);
			}
		}

		public static string lJavaVMOptionen_en
		{
			get
			{
				return Resources.ResourceManager.GetString("lJavaVMOptionen_en", Resources.resourceCulture);
			}
		}

		public static string msgOptionsJVM_de
		{
			get
			{
				return Resources.ResourceManager.GetString("msgOptionsJVM_de", Resources.resourceCulture);
			}
		}

		public static string msgOptionsJVM_en
		{
			get
			{
				return Resources.ResourceManager.GetString("msgOptionsJVM_en", Resources.resourceCulture);
			}
		}

		public static string msgWindowTitle_de
		{
			get
			{
				return Resources.ResourceManager.GetString("msgWindowTitle_de", Resources.resourceCulture);
			}
		}

		public static string msgWindowTitle_en
		{
			get
			{
				return Resources.ResourceManager.GetString("msgWindowTitle_en", Resources.resourceCulture);
			}
		}

		public static string msgWindowWarn_de
		{
			get
			{
				return Resources.ResourceManager.GetString("msgWindowWarn_de", Resources.resourceCulture);
			}
		}

		public static string msgWindowWarn_en
		{
			get
			{
				return Resources.ResourceManager.GetString("msgWindowWarn_en", Resources.resourceCulture);
			}
		}

		public static string rbFullscreen_de
		{
			get
			{
				return Resources.ResourceManager.GetString("rbFullscreen_de", Resources.resourceCulture);
			}
		}

		public static string rbFullscreen_en
		{
			get
			{
				return Resources.ResourceManager.GetString("rbFullscreen_en", Resources.resourceCulture);
			}
		}

		public static string rbWindow_de
		{
			get
			{
				return Resources.ResourceManager.GetString("rbWindow_de", Resources.resourceCulture);
			}
		}

		public static string rbWindow_en
		{
			get
			{
				return Resources.ResourceManager.GetString("rbWindow_en", Resources.resourceCulture);
			}
		}

		public static Bitmap welcomeLogoOut
		{
			get
			{
				return (Bitmap)Resources.ResourceManager.GetObject("welcomeLogoOut", Resources.resourceCulture);
			}
		}

		internal Resources()
		{
		}
	}
}
