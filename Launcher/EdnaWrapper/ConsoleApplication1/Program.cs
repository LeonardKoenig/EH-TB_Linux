using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace ConsoleApplication1
{
	internal class Program
	{
		public struct DISPLAY_DEVICE
		{
			public int cb;

			[MarshalAs(23, SizeConst = 32)]
			public string DeviceName;

			[MarshalAs(23, SizeConst = 128)]
			public string DeviceString;

			public int StateFlags;

			[MarshalAs(23, SizeConst = 128)]
			public string DeviceID;

			[MarshalAs(23, SizeConst = 128)]
			public string DeviceKey;

			public DISPLAY_DEVICE(int flags)
			{
				this.cb = 0;
				this.StateFlags = flags;
				this.DeviceName = new string(' ', 32);
				this.DeviceString = new string(' ', 128);
				this.DeviceID = new string(' ', 128);
				this.DeviceKey = new string(' ', 128);
				this.cb = Marshal.SizeOf(this);
			}
		}

		public struct DEVMODE
		{
			[MarshalAs(23, SizeConst = 32)]
			public string dmDeviceName;

			public short dmSpecVersion;

			public short dmDriverVersion;

			public short dmSize;

			public short dmDriverExtra;

			public int dmFields;

			public short dmOrientation;

			public short dmPaperSize;

			public short dmPaperLength;

			public short dmPaperWidth;

			public short dmScale;

			public short dmCopies;

			public short dmDefaultSource;

			public short dmPrintQuality;

			public short dmColor;

			public short dmDuplex;

			public short dmYResolution;

			public short dmTTOption;

			public short dmCollate;

			[MarshalAs(23, SizeConst = 32)]
			public string dmFormName;

			public short dmUnusedPadding;

			public short dmBitsPerPel;

			public int dmPelsWidth;

			public int dmPelsHeight;

			public int dmDisplayFlags;

			public int dmDisplayFrequency;
		}

		public static string sJVMStartOptions = " -Xmx1024m -Xms1024m -Xincgc -Djava.library.path=\".\" ";

		public static Program.DEVMODE configureDisplay()
		{
			if (Environment.OSVersion.Platform == PlatformID.Win32NT)
			{
				List<Program.DISPLAY_DEVICE> list = new List<Program.DISPLAY_DEVICE>();
				List<Program.DEVMODE> list2 = new List<Program.DEVMODE>();
				Program.DISPLAY_DEVICE dISPLAY_DEVICE = new Program.DISPLAY_DEVICE(0);
				Program.DISPLAY_DEVICE dISPLAY_DEVICE2;
				dISPLAY_DEVICE2.DeviceString = "";
				dISPLAY_DEVICE2.DeviceName = "";
				Program.DEVMODE dEVMODE = default(Program.DEVMODE);
				Program.DEVMODE dEVMODE2 = default(Program.DEVMODE);
				Program.DEVMODE devMode = default(Program.DEVMODE);
				int num = 0;
				int num2 = 0;
				IntPtr intPtr = Program.FindWindow(null, "EdnaWrapper");
				if (intPtr != IntPtr.Zero)
				{
					Program.ShowWindow(intPtr, 0);
				}
				bool flag;
				do
				{
					flag = Program.EnumDisplayDevices(IntPtr.Zero, num, ref dISPLAY_DEVICE, 0);
					if (flag)
					{
						list.Add(dISPLAY_DEVICE);
						if ((dISPLAY_DEVICE.StateFlags & 4) != 0)
						{
							dISPLAY_DEVICE2 = dISPLAY_DEVICE;
						}
					}
					num++;
				} while (flag);
				Console.WriteLine("Main Device: " + dISPLAY_DEVICE2.DeviceString);
				string deviceName = dISPLAY_DEVICE2.DeviceName;
				dEVMODE = default(Program.DEVMODE);
				do
				{
					flag = Program.EnumDisplaySettings(deviceName, num2, ref dEVMODE);
					if (flag)
					{
						list2.Add(dEVMODE);
					}
					num2++;
				} while (flag);
				Program.EnumDisplaySettings(dISPLAY_DEVICE2.DeviceName, -1, ref dEVMODE2);
				Console.WriteLine("Current device mode: " + Program.DevmodeToString(dEVMODE2));
				for (int i = 0; i < list2.Count; i++)
				{
					Program.DEVMODE dEVMODE3 = list2[i];
					if (dEVMODE3.dmPelsWidth == dEVMODE2.dmPelsWidth && dEVMODE3.dmPelsHeight == dEVMODE2.dmPelsHeight &&
					    dEVMODE3.dmDisplayFrequency == dEVMODE2.dmDisplayFrequency && dEVMODE3.dmBitsPerPel == 16)
					{
						devMode = list2[i];
						break;
					}
				}
				Console.WriteLine("Switching to: " + Program.DevmodeToString(devMode) + "...");
				Program.ChangeDisplaySettings(ref devMode, 0);
				Thread.Sleep(1000);
				return dEVMODE2;
			}
			// we are not allowed to return null...
			return new Program.DEVMODE();
		}
		
		private static void Main(string[] args)
		{
			// we need this lateron
			Program.DEVMODE dEVMODE2 = configureDisplay();
			
			bool noComments = false;
			bool accelerated = false;
			for (int j = 0; j < args.Length; j++)
			{
				string expr_1B3 = args[j];
				if (expr_1B3 == "-commentOff")
				{
					noComments = true;
				}
				if (expr_1B3 == "-accelerated")
				{
					accelerated = true;
				}
			}
			Process process = Program.StartEdna(noComments, accelerated);
			if (process != null)
			{
				process.WaitForExit();
				Console.WriteLine("Edna has returned. Resetting display settings...");
			}

			if (Environment.OSVersion.Platform == PlatformID.Win32NT)
			{
				Program.ChangeDisplaySettings(ref dEVMODE2, 0);
			}
		}

		private static string DevmodeToString(Program.DEVMODE devMode)
		{
			return string.Concat(new string[]
			{
				devMode.dmPelsWidth.ToString(),
				" x ",
				devMode.dmPelsHeight.ToString(),
				", ",
				devMode.dmBitsPerPel.ToString(),
				" bits, ",
				devMode.dmDisplayFrequency.ToString(),
				" Hz"
			});
		}

		public static Process StartEdna(bool noComments, bool accelerated)
		{
			string javabin;
			if (Environment.OSVersion.Platform == PlatformID.Unix)
			{
				javabin = "java";
			}
			else
			{
				string javaInstallationPath = Program.GetJavaInstallationPath();
				if (javaInstallationPath == null)
				{
					Console.WriteLine("Error! Java not found!");
					return null;
				}
				javabin = Path.Combine(javaInstallationPath, "bin\\javaw.exe");
				if (!File.Exists(javabin))
				{
					Console.WriteLine("Error! Java not found!");
					return null;
				}
			}
			string text = javabin;
			string text2 = Program.sJVMStartOptions;
			if (accelerated)
			{
				text2 += "-Dsun.java2d.opengl=True ";
			}
			string text3 = text2 + "-jar Edna.jar";
			if (noComments)
			{
				text3 += " -commentOff";
			}
			if (!File.Exists("Edna.jar"))
			{
				Console.WriteLine("Error! Edna not found!");
				return null;
			}
			Console.WriteLine("Running: " + text + text3);
			return Process.Start(text, text3);
		
		}

		private static string GetJavaInstallationPath()
		{
			string javaInstallationPath = Program.GetJavaInstallationPath(false, true);
			if (javaInstallationPath != null)
			{
				return javaInstallationPath;
			}
			javaInstallationPath = Program.GetJavaInstallationPath(false, false);
			if (javaInstallationPath != null)
			{
				return javaInstallationPath;
			}
			javaInstallationPath = Program.GetJavaInstallationPath(true, true);
			if (javaInstallationPath != null)
			{
				return javaInstallationPath;
			}
			return Program.GetJavaInstallationPath(true, false);
		}

		private static string GetJavaInstallationPath(bool jdk, bool x64)
		{
			string text;
			if (jdk)
			{
				text = "SOFTWARE\\JavaSoft\\Java Development Kit\\";
			}
			else
			{
				text = "SOFTWARE\\JavaSoft\\Java Runtime Environment\\";
			}
			RegistryKey registryKey;
			if (x64)
			{
				registryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64).OpenSubKey(text);
			}
			else
			{
				registryKey = Registry.LocalMachine.OpenSubKey(text);
			}
			if (registryKey != null)
			{
				RegistryKey registryKey2 = registryKey.OpenSubKey("1.8");
				if (registryKey2 != null)
				{
					return registryKey2.GetValue("JavaHome").ToString();
				}
			}
			return null;
		}

		[DllImport("User32.dll")]
		private static extern bool EnumDisplayDevices(IntPtr lpDevice, int iDevNum, ref Program.DISPLAY_DEVICE lpDisplayDevice, int dwFlags);

		[DllImport("User32.dll")]
		private static extern bool EnumDisplaySettings(string devName, int modeNum, ref Program.DEVMODE devMode);

		[DllImport("user32.dll")]
		public static extern int ChangeDisplaySettings(ref Program.DEVMODE devMode, int flags);

		[DllImport("user32.dll")]
		public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

		[DllImport("user32.dll")]
		private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
	}
}
