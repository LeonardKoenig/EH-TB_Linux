#! /bin/bash
cd "$(dirname "$0")"
CP_LIB=$(find lib/ -name '*.jar' -printf '%h/%f:')
CP_DATA=$(find data/ -name '*.jar' -printf '%h/%f:')
export LD_LIBRARY_PATH=".:$LD_LIBRARY_PATH"
MEMMAX=1024 # 512 for lower res
MEMINIT=1024
# Options of Edna.jar:
# -commentOff
# -fullscreen -f
# -window -w
# -language [de/en]
# -log [-script] [-visual]
#
# Acceleration:
# -Dsun.java2d.opengl=True (in fullscreen might need switching to window and
# back)
# Heap of 1GB needed instead of 512MB!
java -cp "$CP_LIB$CP_DATA" -Xmx${MEMMAX}m -Xms${MEMINIT}m -Xincgc \
	-Dsun.java2d.opengl=True \
	-jar Edna.jar \
	-fullscreen -language de -log -script -visual
